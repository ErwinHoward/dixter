# Dixter 

Project aims to be able to translate most of the modern languages and dead ones like 
Indo-European, Avestan, Old Norse and some others.

Includes speech generation closer to natural human speech 
and it's will be able to learn from voices that user supplies.
 
Includes sign language recognizer module for disabled people.

## Translator structure

Project consists of several parts:

**Language structures** 

U
_verb_, _noun_, _pronoun_, _adjective_, _adverb_

**Machine Learning**

The project aims to instanly translate words, sentences like _Google Translate_, but with open source.

Additionally project provides GUI module for easy translation also a dictionary generated from ML module.


**Author:** **Alvin Ahmadov**
